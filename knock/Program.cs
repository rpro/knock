﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Net;


namespace knock
{
    internal class Program
    {
        static void ShowExample(string err)
        {
            Console.WriteLine(err);
            Console.WriteLine();
            Console.WriteLine("Usage: knock <ip or hostname> d:<delay> p:<protocol> [port 1] [port 2] [port 3] [port ...]");
            Console.WriteLine("Example:  knock 192.168.2.1 d:100 p:tcp 7000 8000 9000");
        }

        static void Main(string[] args)
        {
            // Make sure we have enough arguments for hostname, delay, protocol and a port
            if (args.Length < 4)
            {
                ShowExample("Insufficient arguments.");
                Environment.Exit(0);
            }

            // Get the delay
            int delay = 100; // sane default 
            if (args[1].ToString().Substring(0,2).ToLower() == "d:" )
            {
                try {
                    delay = Int32.Parse(args[1].ToString().Substring(2, args[1].ToString().Length - 2));
                } catch {
                    ShowExample("Invalid delay");
                    Environment.Exit(0);
                }
            } else {
                ShowExample("Delay not specified");
                Environment.Exit(0);
            }


            // Get the protocol to use
            ProtocolType protocol = new ProtocolType(); 
            if (args[2].ToString().Substring(0, 2).ToLower() == "p:")
            {
                switch (args[2].ToString().Substring(2, args[2].ToString().Length - 2))
                {
                    case "tcp":
                        protocol = ProtocolType.Tcp;
                    break;

                    case "udp":
                        protocol = ProtocolType.Udp;
                    break;

                    default:
                            ShowExample("Invlaid protocol - valid protocols are p:tcp p:udp");
                            Environment.Exit(0);
                    break;
                }   
            } else {
                ShowExample("Protocol not specified");
                Environment.Exit(0);
            }


            // Make sure all arguments after hostname and delay are numeric ports
            Regex regex = new Regex(@"^\d+$");
            for (int i = 3; i < args.Length; i++)
            {
                if (!regex.IsMatch(args[i]))
                {
                    ShowExample("Non-numeric ports");
                    Environment.Exit(0);
                }
            }

            // Add all the ports to the Endpoints List
            IPHostEntry ipHostInfo = new IPHostEntry();
            try {
                ipHostInfo = Dns.GetHostEntry(args[0]);
            } catch {
                Console.WriteLine("Host Unreachable.");
                Environment.Exit(0);
            }
            
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            List<IPEndPoint> EndPoints = new List<IPEndPoint>();
            for (int i = 3; i < args.Length; i++)
            {
                try {
                    EndPoints.Add(new IPEndPoint(ipAddress, Int32.Parse(args[i])));
                } catch   {
                    ShowExample("Error adding ports");
                    Environment.Exit(0);
                }
            }

            // Try to connect to each EndPoint              
            foreach (IPEndPoint EndPoint in EndPoints)
            {
                try {
                    Console.WriteLine("Knocking " + args[2] + " " + args[0].ToString() + ":" + EndPoint.Port.ToString());
                    Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, protocol); 
                    IAsyncResult result = sender.BeginConnect(EndPoint, null, null);
                    bool success = result.AsyncWaitHandle.WaitOne(delay, true);  
                    if (sender.Connected)
                    {
                        sender.EndConnect(result);
                    }
                    else
                    {
                        sender.Close();
                    }

                } catch {
                    // do nothing on connect fail
                }
            }

            // Free mem
            EndPoints.Clear();
            EndPoints = null;

        }
    }
}
