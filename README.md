**knock**

This is a basic c# solution implimentation of a port knock client for windows.  Intended for use with services like knockd.

Usage: 
```
knock <ip or hostname> d:<delay> p:<protocol> [port 1] [port 2] [port 3] [port ...]
```

Example:
```
knock 192.168.2.1 d:100 p:tcp 7000 8000 9000
```
